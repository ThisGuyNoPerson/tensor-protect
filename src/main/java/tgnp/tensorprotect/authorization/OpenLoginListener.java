package tgnp.tensorprotect.authorization;

import com.nickuc.openlogin.bukkit.api.events.AsyncLoginEvent;
import com.nickuc.openlogin.bukkit.api.events.AsyncRegisterEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import tgnp.tensorprotect.TensorProtect;

public class OpenLoginListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onAsyncRegister(AsyncRegisterEvent event) {
        TensorProtect.showCaptcha(event.getPlayer(), 2);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onAsyncLogin(AsyncLoginEvent event) {
        TensorProtect.showCaptcha(event.getPlayer(), 2);
    }

}
