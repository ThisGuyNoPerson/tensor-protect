package tgnp.tensorprotect.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tgnp.tensorprotect.ConfigDataStorage;
import tgnp.tensorprotect.TensorProtect;

public class TensorProtectCommand implements CommandExecutor {

    private void sendMessage(CommandSender sender, String string) {
        if(sender instanceof Player)
            sender.sendMessage(string);
        else
            TensorProtect.getPlugin().getLogger().info(string);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length > 0) {
            if(strings[0].contentEquals("reload")) {
                sendMessage(commandSender, "§7Reloading...");
                if(ConfigDataStorage.reload())
                    sendMessage(commandSender, "§aSuccessfully reloaded.");
                else
                    sendMessage(commandSender, "§cFailed to reload config.");
            } else if(strings[0].contentEquals("show")) {
                Player player = strings.length > 1 ? Bukkit.getPlayerExact(strings[1]) : null;
                if(player != null) {
                    TensorProtect.showCaptcha(player, 2);
                }else
                    sendMessage(commandSender, "§cPlayer not found.");
            }
        }else {
            sendMessage(commandSender, "§7/tensorprotect reload");
            sendMessage(commandSender, "§7/tensorprotect show §f[player]");
        }
        return true;
    }

}
