package tgnp.tensorprotect;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigDataStorage {

    public static String kickMessage;
    public static String wrongItemTitle;
    public static String wrongItemLoreLine;
    public static String correctItemTitle;
    public static String correctItemLoreLine;
    public static int timeoutTicks;
    public static boolean joinMessagesEnabled;
    public static boolean leaveMessagesEnabled;

    public static boolean reload() {
        try {
            TensorProtect.getPlugin().reloadConfig();
            FileConfiguration config = TensorProtect.getPlugin().getConfig();
            kickMessage = config.getString("kick-message");
            wrongItemTitle = config.getString("wrong-item-title");
            wrongItemLoreLine = config.getString("wrong-item-lore-line");
            correctItemTitle = config.getString("correct-item-title");
            correctItemLoreLine = config.getString("correct-item-lore-line");
            timeoutTicks = config.getInt("timeout-ticks");
            joinMessagesEnabled = config.getBoolean("join-messages-enabled");
            leaveMessagesEnabled = config.getBoolean("leave-messages-enabled");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
