package tgnp.tensorprotect;

public class MinecraftStringParser {

    public static String parse(String source) {
        StringBuilder parsedString = new StringBuilder();
        String[] lines = source.split("\\\\n");
        for (String line : lines) {
            line = line.replaceAll("&", "§");
            parsedString.append(line).append("\n");
        }
        return parsedString.toString();
    }

}
