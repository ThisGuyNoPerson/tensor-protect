package tgnp.tensorprotect;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

public class Task implements Listener {

    private final Plugin plugin = TensorProtect.getPlugin();

    public Task(Runnable runnable, long delay) {
        if(plugin.isEnabled())
            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, runnable, delay);
        else
            runnable.run();
    }

}
