package tgnp.tensorprotect.authorization;

import fr.xephi.authme.events.LoginEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import tgnp.tensorprotect.TensorProtect;

public class AuthMeListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerLogin(LoginEvent event) {
        TensorProtect.showCaptcha(event.getPlayer(), 2);
    }

}
