package tgnp.tensorprotect;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

public class PluginListener implements Listener {

    private void kick(Player player) {
        if(player != null)
            player.kickPlayer(MinecraftStringParser.parse(ConfigDataStorage.kickMessage));
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if(!ConfigDataStorage.joinMessagesEnabled)
            event.setJoinMessage("");

        if(!Bukkit.getServer().getPluginManager().isPluginEnabled("AuthMe") &&
                !Bukkit.getServer().getPluginManager().isPluginEnabled("OpeNLogin")) {
            TensorProtect.showCaptcha(event.getPlayer(), 10);
        }

    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if(!ConfigDataStorage.leaveMessagesEnabled)
            event.setQuitMessage("");
        DataStorage.captchaPlayers.remove(event.getPlayer());
        DataStorage.inventoryNodes.values().remove(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onInventoryClose(InventoryCloseEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer())) {
            new Task(() -> {
                Player player = Bukkit.getServer().getPlayer(event.getPlayer().getName());
                kick(player);
            }, 2);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = Bukkit.getServer().getPlayer(event.getEntity().getName());
            if(DataStorage.captchaPlayers.contains(player))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = Bukkit.getServer().getPlayer(event.getEntity().getName());
            if(DataStorage.captchaPlayers.contains(player))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer())) {
            event.setCancelled(true);
            kick(event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer())) {
            event.setCancelled(true);
            kick(event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer())) {
            event.setCancelled(true);
            kick(event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerPickupArrow(PlayerPickupArrowEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if(DataStorage.captchaPlayers.contains(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent event) {
        if(DataStorage.inventoryNodes.containsKey(event.getInventory())) {
            event.setCancelled(true);
            if (event.getClickedInventory() == null)
                return;
            ItemStack item = event.getCurrentItem();
            if(item == null)
                return;
            new Task(() -> {
                Player player = DataStorage.inventoryNodes.get(event.getInventory());
                if(item != null && item.getData().getData() == (byte) 15) {
                    kick(player);
                }else if(item != null && item.getData().getData() == (byte) 5) {
                    DataStorage.captchaPlayers.remove(player);
                    DataStorage.inventoryNodes.values().remove(player);
                    player.closeInventory(InventoryCloseEvent.Reason.PLAYER);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                }
            }, 2);
        }
    }

}
