package tgnp.tensorprotect;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import tgnp.tensorprotect.authorization.AuthMeListener;
import tgnp.tensorprotect.authorization.OpenLoginListener;
import tgnp.tensorprotect.captcha.gui.GuiCaptcha;
import tgnp.tensorprotect.commands.TensorProtectCommand;
import tgnp.tensorprotect.commands.TensorProtectTabCompleter;
import tgnp.tensorprotect.metrics.Metrics;

public final class TensorProtect extends JavaPlugin {

    private static TensorProtect plugin;

    private void enablePlugin(Plugin plugin) {
        if(plugin != null)
            getPluginLoader().enablePlugin(plugin);
    }

    private void initHooks() {

        enablePlugin(Bukkit.getServer().getPluginManager().getPlugin("AuthMe"));
        enablePlugin(Bukkit.getServer().getPluginManager().getPlugin("OpeNLogin"));

        if(Bukkit.getServer().getPluginManager().isPluginEnabled("AuthMe")) {
            getLogger().info("AuthMe is found.");
            getServer().getPluginManager().registerEvents(new AuthMeListener(), this);
        } else
            getLogger().info("AuthMe is not found!");
        if(Bukkit.getServer().getPluginManager().isPluginEnabled("OpeNLogin")) {
            getLogger().info("OpeNLogin is found.");
            getServer().getPluginManager().registerEvents(new OpenLoginListener(), this);
        } else
            getLogger().info("OpeNLogin is not found!");

    }

    @Override
    public void onEnable() {
        plugin = this;

        // Include plugin metrics
        Metrics metrics = new Metrics(this, 20293); // Do not change this number

        // Load config
        this.saveDefaultConfig();
        ConfigDataStorage.reload();

        // Init hooks
        initHooks();

        // Init main event handler
        getServer().getPluginManager().registerEvents(new PluginListener(), this);

        // Init commands
        getCommand("tensorprotect").setExecutor(new TensorProtectCommand());
        getCommand("tensorprotect").setTabCompleter(new TensorProtectTabCompleter());

        getLogger().info("Enabled.");
    }

    @Override
    public void onDisable() {
        getLogger().info("Disabled.");
    }

    public static TensorProtect getPlugin() {
        return plugin;
    }

    public static void showCaptcha(Player player, int delayToShow) {
        if(DataStorage.captchaPlayers.contains(player))
            return;
        DataStorage.captchaPlayers.add(player);

        new Task(() -> {
            if(DataStorage.captchaPlayers.contains(player)) {
                player.kickPlayer(MinecraftStringParser.parse(ConfigDataStorage.kickMessage));
            }
        }, ConfigDataStorage.timeoutTicks);

        new Task(() -> {
            GuiCaptcha captcha = new GuiCaptcha(player);
            captcha.open();
            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
        }, delayToShow);
    }

}
