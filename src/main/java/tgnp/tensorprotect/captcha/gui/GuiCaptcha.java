package tgnp.tensorprotect.captcha.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tgnp.tensorprotect.ConfigDataStorage;
import tgnp.tensorprotect.DataStorage;
import tgnp.tensorprotect.MinecraftStringParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GuiCaptcha {

    private final Player player;
    private final Inventory inventory;

    public GuiCaptcha(Player player) {
        this.player = player;
        this.inventory = Bukkit.createInventory(player, 27, "Tensor Captcha");
    }

    public void open() {

        for (int i = 0; i < 27; i++) {
            List<String> backgroundLore = new ArrayList<String>();
            backgroundLore.add(MinecraftStringParser.parse(ConfigDataStorage.wrongItemLoreLine));
            ItemStack background = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)15);
            background.setLore(backgroundLore);
            ItemMeta backgroundMeta = background.getItemMeta();
            backgroundMeta.setDisplayName(MinecraftStringParser.parse(ConfigDataStorage.wrongItemTitle));
            background.setItemMeta(backgroundMeta);
            this.inventory.setItem(i, background);
        }

        List<String> backgroundLore = new ArrayList<String>();
        backgroundLore.add(MinecraftStringParser.parse(ConfigDataStorage.correctItemLoreLine));
        ItemStack background = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)5);
        background.setLore(backgroundLore);
        ItemMeta backgroundMeta = background.getItemMeta();
        backgroundMeta.setDisplayName(MinecraftStringParser.parse(ConfigDataStorage.correctItemTitle));
        background.setItemMeta(backgroundMeta);

        int rand = new Random().nextInt(26);
        this.inventory.setItem(rand, background);

        DataStorage.inventoryNodes.put(this.inventory, this.player);
        this.player.openInventory(this.inventory);
    }

}
