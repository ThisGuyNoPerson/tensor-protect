package tgnp.tensorprotect.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TensorProtectTabCompleter implements TabCompleter {

    private List<String> mainCommandArguments = new ArrayList<String>();

    public TensorProtectTabCompleter() {
        this.mainCommandArguments.add("reload");
        this.mainCommandArguments.add("show");
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length > 0) {
            if(strings.length == 2 && strings[0].contentEquals("show")) {
                List<String> players = new ArrayList<String>();
                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    players.add(player.getName());
                }
                return players;
            }
            if(strings.length == 1)
                return this.mainCommandArguments;
            else
                return new ArrayList<String>();
        }else {
            return new ArrayList<String>();
        }
    }

}
